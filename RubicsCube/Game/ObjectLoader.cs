﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
/*
 * Made by Rick van Sloten
 * Simple .OBJ parser
 */
    class ObjectLoader
    {
        IEnumerable<string> _objData;

        List<Vector3f> _vertices = new List<Vector3f>();
        List<int> _order = new List<int>();
        List<List<int>> _faces = new List<List<int>>();

        public ObjectLoader()
        {

        }

        public void LoadObj(string path)
        {
            _objData = File.ReadAllLines(path);

            foreach (string line in _objData)
            {
                ExtractVertices(line);
            }
        }

        private void ExtractVertices(string line)
        {
            string[] data = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            if (data.Length > 0)
            {
                switch (data[0])
                {
                    case "v":
                        _vertices.Add(new Vector3f(float.Parse(data[1], System.Globalization.CultureInfo.InvariantCulture), float.Parse(data[2], System.Globalization.CultureInfo.InvariantCulture), float.Parse(data[3], System.Globalization.CultureInfo.InvariantCulture)));
                        break;

                    case "f":
                        List<int> order = new List<int>();
                        for (int i = 0; i < data.Length; i++)
                        {
                            string[] chars = data[i].Split('/');
                            if (chars[0] != "f")
                            {
                                order.Add(Convert.ToInt32(chars[0]));
                            }
                        }
                        _faces.Add(order);
                        break;
                }
            }
        }

        public void OutPutVertices()
        {
            foreach (Vector3f v in _vertices)
            {
                Console.WriteLine("LOADING VERTEX: X: {0}, Y:{1}, Z:{2}", v.X, v.Y, v.Z);
            }

            foreach (List<int> list in _faces)
            {
                Console.Write("LOADING FACE: ");
                foreach (int face in list)
                {
                    Console.Write(" " + face);
                }
                Console.Write("\n");
            }
        }

        public List<Vector3f> GetVertices()
        {
            return _vertices;
        }

        public List<List<int>> GetFaces()
        {
            return _faces;
        }
    }
}
