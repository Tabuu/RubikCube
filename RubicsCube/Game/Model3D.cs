﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    /*
     * Made by Rick van Sloten
     */
    class Model3D
    {
        GameEngine _gameEngine;

        Color _color = Color.AppelBlauwZeeGroen;

        Vector2f _location = new Vector2f(640, 360);

        List<Vector2f[]> _displayPoints = new List<Vector2f[]>();
        List<Vector3f[]> _meshes = new List<Vector3f[]>();

        int _zCenter = 1500;
        int _scale = 300;

        public Model3D()
        {
            _gameEngine = GameEngine.GetInstance();
        }

        public void AddMesh(Vector3f[] mesh)
        {
            _meshes.Add(mesh);
        }

        private void ConvertTo2D()
        {
            _displayPoints.Clear();
            foreach (Vector3f[] mesh in _meshes)
            {
                Vector2f[] displaypunten = new Vector2f[mesh.Length];
                for (int i = 0; i < mesh.Length; i++)
                {
                    Vector3f punt = mesh[i];
                    float scale = _scale / (_scale + punt.Z + _zCenter);

                    displaypunten[i].X = punt.X * scale;
                    displaypunten[i].Y = punt.Y * scale;
                }
                _displayPoints.Add(displaypunten);
            }
        }

        public void Update()
        {
            ConvertTo2D();
        }

        public void Zoom(int zoom)
        {
            _zCenter += zoom;
        }

        public void Transform(float x, float y, float z)
        {
            foreach (Vector3f[] mesh in _meshes)
            {
                for (int i = 0; i < mesh.Count(); i++)
                {
                    mesh[i].X += x;
                    mesh[i].Y += y;
                    mesh[i].Z += z;
                }
            }
        }

        public void RotateX(double hoek)
        {
            double cos = Math.Cos(hoek);
            double sin = Math.Sin(hoek);

            foreach (Vector3f[] mesh in _meshes)
            {
                for (int i = 0; i < mesh.Count(); i++)
                {
                    double y = mesh[i].Y * cos - mesh[i].Z * sin;
                    double z = mesh[i].Z * cos + mesh[i].Y * sin;
                    mesh[i].Y = (float)y;
                    mesh[i].Z = (float)z;
                }
            }
        }
        public void RotateY(double hoek)
        {
            double cos = Math.Cos(hoek);
            double sin = Math.Sin(hoek);

            foreach (Vector3f[] mesh in _meshes)
            {
                for (int i = 0; i < mesh.Count(); i++)
                {
                    double x = mesh[i].X * cos - mesh[i].Z * sin;
                    double z = mesh[i].Z * cos + mesh[i].X * sin;
                    mesh[i].X = (float)x;
                    mesh[i].Z = (float)z;
                }
            }
        }
        public void RotateZ(double hoek)
        {
            double cos = Math.Cos(hoek);
            double sin = Math.Sin(hoek);

            foreach (Vector3f[] mesh in _meshes)
            {
                for (int i = 0; i < mesh.Count(); i++)
                {
                    double x = (mesh[i].X * cos) - (mesh[i].Y * sin);
                    double y = (mesh[i].Y * cos) + (mesh[i].X * sin);
                    mesh[i].X = (float)x;
                    mesh[i].Y = (float)y;
                }
            }
        }

        private void DrawLines(Vector2f[] punten)
        {
            for (int i = 0; i < punten.Length - 1; i++)
            {
                _gameEngine.DrawLine(punten[i].X + _location.X, punten[i].Y + _location.Y, punten[i + 1].X + _location.X, punten[i + 1].Y + _location.Y);
            }
            _gameEngine.DrawLine(punten[0].X + _location.X, punten[0].Y + _location.Y, punten[punten.Length - 1].X + _location.X, punten[punten.Length - 1].Y + _location.Y);
        }

        public void Draw()
        {
            _gameEngine.SetColor(_color);
            foreach (Vector2f[] var2DPunten in _displayPoints)
            {
                DrawLines(var2DPunten);
            }
        }
    }
}
