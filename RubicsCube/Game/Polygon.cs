﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    class Polygon
    {


        GameEngine _gameEngine;

        Vector2f _leftUp;
        Vector2f _rightUp;
        Vector2f _leftDown;
        Vector2f _rightDown;

        public Polygon()
        {
            _gameEngine = GameEngine.GetInstance();
        }

        public List<Vector2f> GetLocations()
        {
            List<Vector2f> locationList = new List<Vector2f>();
            locationList.Add(_leftUp);
            locationList.Add(_rightUp);
            locationList.Add(_leftDown);
            locationList.Add(_rightDown);

            return locationList;
        }

        public void Draw(
            float leftUpX, float leftUpY,
            float rightUpX, float rightUpY,
            float leftDownX, float leftDownY, 
            float rightDownX, float rightDownY)
        {
            _leftUp.X = leftUpX;
            _leftUp.Y = leftUpY;

            _rightUp.X = rightUpX;
            _rightUp.Y = rightUpY;

            _leftDown.X = leftDownX;
            _leftDown.Y = leftDownY;

            _rightDown.X = rightDownX;
            _rightDown.Y = rightDownY;


            _gameEngine.DrawLine(_leftUp, _rightUp);
            _gameEngine.DrawLine(_rightUp, _rightDown);
            _gameEngine.DrawLine(_rightDown, _leftDown);
            _gameEngine.DrawLine(_leftDown, _leftUp);
        }
    }
}
