﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    public class RubicsCube : AbstractGame
    {
        Model3D _cubeModel;
        Model3D _model;
        ObjectLoader _objLoader;

        float _mousex;
        float _mousey;

        public override void GameStart()
        {
            _mousex = GAME_ENGINE.GetMousePosition().X;
            _mousey = GAME_ENGINE.GetMousePosition().Y;
            GAME_ENGINE.SetBackgroundColor(0, 0, 0);
            _objLoader = new ObjectLoader();
            _cubeModel = new Model3D();
            _model = new Model3D();

            LoadCube();
            _objLoader.LoadObj("../../Assets/example.obj");
            LoadModel();

            //_objLoader.OutPutVertices();
        }

        public override void GameEnd()
        {

        }

        public override void Update()
        {
            Controls();
            _cubeModel.Update();
            _model.Update();
        }

        public override void Paint()
        {
            _cubeModel.Draw();
            _model.Draw();
        }

        public void Controls()
        {
            
            if (GAME_ENGINE.GetMouseButton(0) && (GAME_ENGINE.GetMousePosition().X != _mousex || GAME_ENGINE.GetMousePosition().Y != _mousex))
            {
                _model.RotateX((GAME_ENGINE.GetMousePosition().Y - _mousey) / 50);
                _model.RotateY((GAME_ENGINE.GetMousePosition().X - _mousex) / 50);

                _cubeModel.RotateX((GAME_ENGINE.GetMousePosition().Y - _mousey) / 50);
                _cubeModel.RotateY((GAME_ENGINE.GetMousePosition().X - _mousex) / 50);
            }
            _mousex = GAME_ENGINE.GetMousePosition().X;
            _mousey = GAME_ENGINE.GetMousePosition().Y;

            if (GAME_ENGINE.GetKey(Key.Left))
            {
                if (GAME_ENGINE.GetKey(Key.ControlKey))
                {
                    _cubeModel.RotateY(-0.01);
                    _model.RotateY(-0.01);
                }
                else
                {
                    _model.Transform(10, 0, 0);
                    _cubeModel.Transform(10, 0, 0);
                }             
            }
            if (GAME_ENGINE.GetKey(Key.Up))
            {
                if (GAME_ENGINE.GetKey(Key.ControlKey))
                {
                    _cubeModel.RotateX(-0.01);
                    _model.RotateX(-0.01);
                }
                else
                {
                    _model.Transform(0, 10, 0);
                    _cubeModel.Transform(0, 10, 0);
                }      
            }

            if (GAME_ENGINE.GetKey(Key.Right))
            {
                if (GAME_ENGINE.GetKey(Key.ControlKey))
                {
                    _cubeModel.RotateY(0.01);
                    _model.RotateY(0.01);
                }
                else
                {
                    _model.Transform(-10, 0, 0);
                    _cubeModel.Transform(-10, 0, 0);
                }
            }

            if (GAME_ENGINE.GetKey(Key.Down))
            {
                if (GAME_ENGINE.GetKey(Key.ControlKey))
                {
                    _cubeModel.RotateX(0.01);
                    _model.RotateX(0.01);
                }
                else
                {
                    _model.Transform(0, -10, 0);
                    _cubeModel.Transform(0, -10, 0);
                }
            }
            if (GAME_ENGINE.GetKey(Key.Add))
            {
                //_model.Transform(0, 0, 10);
                _model.Zoom(10);
                //_cubeModel.Transform(0, 0, 10);
                _cubeModel.Zoom(10);
            }
            if (GAME_ENGINE.GetKey(Key.Subtract))
            {
               // _model.Transform(0, 0, -10);
                _model.Zoom(-10);
               //_cubeModel.Transform(0, 0, -10);
                _cubeModel.Zoom(-10);
            }
        }

        public void LoadCube()
        {
            #region Ribbs
            _cubeModel.AddMesh(new Vector3f[] //DOWN
          {
                 new Vector3f(-500, 500, -500),
                 new Vector3f(500, 500 , -500),
                 new Vector3f(500, 500, 500),
                 new Vector3f(-500, 500, 500),
          });

            _cubeModel.AddMesh(new Vector3f[] //TOP
            {
                new Vector3f(-500, -500, -500),
                new Vector3f(500, -500 , -500),
                new Vector3f(500, -500, 500),
                new Vector3f(-500, -500, 500),
            });

            _cubeModel.AddMesh(new Vector3f[] //LEFT
            {
                new Vector3f(500, -500, 500),
                new Vector3f(500, -500 , -500),
                new Vector3f(500, 500, -500),
                new Vector3f(500, 500, 500),
            });

            _cubeModel.AddMesh(new Vector3f[] //RIGHT
            {
                new Vector3f(-500, -500, 500),
                new Vector3f(-500, -500 , -500),
                new Vector3f(-500, 500, -500),
                new Vector3f(-500, 500, 500),
            });

            _cubeModel.AddMesh(new Vector3f[] //BACK
            {
                new Vector3f(-500, 500, 500),
                new Vector3f(500, 500, 500),
                new Vector3f(500, -500, 500),
                new Vector3f(-500, -500, 500),
            });

            _cubeModel.AddMesh(new Vector3f[] //FRONT
            {
                new Vector3f(-500, 500, -500),
                new Vector3f(500, 500, -500),
                new Vector3f(500, -500, -500),
                new Vector3f(-500, -500, -500),
            });
            #endregion
        }

        public void LoadModel()
        {
            List<Vector3f> v3 = new List<Vector3f>();
            List<Vector3f> vectice = _objLoader.GetVertices();
            for (int i = 0; i < _objLoader.GetVertices().Count; i++)
            {
                v3.Add(vectice[i]);                
            }
            _model.AddMesh(v3.ToArray());
        }
    }
}
